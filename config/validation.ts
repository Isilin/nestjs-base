import * as Joi from 'joi';

export const validationSchema = Joi.object({
  NODE_ENV: Joi.string().valid('development', 'production', 'test'),
  PORT: Joi.number().required(),
  ROOT: Joi.string().required(),
  SSL_ENABLED: Joi.boolean().required(),
  SSL_KEY: Joi.string(),
  SSL_CERT: Joi.string(),
});
