export const configuration = () => ({
  NODE_ENV: process.env.NODE_ENV,
  port: parseInt(process.env.PORT, 10) || 3001,
  rootPrefixe: process.env.ROOT || 'api',
  sslEnabled: process.env.SSL_ENABLED || false,
  sslKey: process.env.SSL_KEY || '',
  sslCert: process.env.SSL_CERT || '',
});
