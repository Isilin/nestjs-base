# NestJS Base

## Description

A base NestJS project, with usual features to develop a strong API.

## Installation

```bash
$ yarn
```

## Set the configuration

In `config/env`, copy the `.env` file to a new one name `.development.env`. You can also do that for other environments (staging, production).
Configure the settings of your file as you want.

```dotenv
PORT=3001
ROOT=api
...
```

### Use HTTPS on local

First you need to generate your self signed certificate :

```bash
$ npm add -g mkcert
$ mkcert create-ca
```

Then, place your `*.crt` and `*.key` files generated in the folder your want. Finally, modify your local `.env` file to access the correct path. The path have to be relative to `[...]\nestjs-base\src` .

```dotenv
SSL_ENABLED=true
SSL_KEY=../config/ca.key
SSL_CERT=../config/ca.crt
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## License

Project is under [GNU-GPL v3](LICENSE.md)

## Project status

This is under development, and should not be used for production yet.
