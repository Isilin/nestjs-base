import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { map, Observable } from 'rxjs';

@Injectable()
export class EmptyResultInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((data) => {
        if (
          (data instanceof Array && !data.length) ||
          context.switchToHttp().getRequest().method === 'DELETE'
        ) {
          context.switchToHttp().getResponse().status(204);
        }
        return data;
      }),
    );
  }
}
