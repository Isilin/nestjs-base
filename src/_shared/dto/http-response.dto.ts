import { ApiProperty } from '@nestjs/swagger';

export class NoContentDTO {
  @ApiProperty({
    example: '204',
    description: 'The HTTP status code of the error.',
  })
  statusCode: number;

  @ApiProperty({
    example: 'No Content Exception',
    description: 'The type of error returned.',
  })
  message: string;

  @ApiProperty({
    example: '2021-11-19T15:06:42.290Z',
    description: 'The timestamp of the request error.',
  })
  timestamp: string;

  @ApiProperty({
    example: '/api/v1/grids/Islands',
    description: 'The endpoint called.',
  })
  path: string;

  @ApiProperty({
    example: 'No Content',
    description: 'Detailed error about the request.',
    type: String,
  })
  errors: string;
}

export class BadRequestDTO {
  @ApiProperty({
    example: '400',
    description: 'The HTTP status code of the error.',
  })
  statusCode: number;

  @ApiProperty({
    example: 'Bad Request Exception',
    description: 'The type of error returned.',
  })
  message: string;

  @ApiProperty({
    example: '2021-11-19T15:06:42.290Z',
    description: 'The timestamp of the request error.',
  })
  timestamp: string;

  @ApiProperty({
    example: '/api/v1/grids/Islands',
    description: 'The endpoint called.',
  })
  path: string;

  @ApiProperty({
    example:
      '[\
        "Missing password"\
      ]',
    description: 'A list of detailed error about the request.',
    type: [String],
  })
  errors: string;
}

export class UnauthorizedDTO {
  @ApiProperty({
    example: '401',
    description: 'The HTTP status code of the error.',
  })
  statusCode: number;

  @ApiProperty({
    example: 'Unhautorized',
    description: 'The type of error returned.',
  })
  message: string;

  @ApiProperty({
    example: '2021-11-19T15:06:42.290Z',
    description: 'The timestamp of the request error.',
  })
  timestamp: string;

  @ApiProperty({
    example: '/api/v1/grids/Islands',
    description: 'The endpoint called.',
  })
  path: string;

  @ApiProperty({
    example: 'Unauthorized',
    description: 'Detailed error about the request.',
    type: String,
  })
  errors: string;
}

export class NotFoundDTO {
  @ApiProperty({
    example: '404',
    description: 'The HTTP status code of the error.',
  })
  statusCode: number;

  @ApiProperty({
    example: 'Not found',
    description: 'The type of error returned.',
  })
  message: string;

  @ApiProperty({
    example: '2021-11-19T15:06:42.290Z',
    description: 'The timestamp of the request error.',
  })
  timestamp: string;

  @ApiProperty({
    example: '/api/v1/grids/Islands',
    description: 'The endpoint called.',
  })
  path: string;

  @ApiProperty({
    example: 'Not found',
    description: 'Detailed error about the request.',
    type: String,
  })
  errors: string;
}
