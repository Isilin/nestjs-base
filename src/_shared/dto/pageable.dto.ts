import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsInt,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';

export enum Direction {
  ASC = 'ASC',
  DESC = 'DESC',
}

export class PageQueryParamDTO {
  @ApiPropertyOptional({
    example: 1,
    description: 'Page parameter for pagination.',
    minimum: 1,
    default: 1,
  })
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @IsOptional()
  page?: number = 1;

  @ApiPropertyOptional({
    example: 10,
    description: 'Limit parameter for pagination.',
    minimum: 1,
    maximum: 50,
    default: 10,
  })
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @Max(50)
  @IsOptional()
  limit?: number = 10;

  get skip(): number {
    return (this.page - 1) * this.limit;
  }

  @ApiPropertyOptional({
    example: 'name,asc',
    description:
      'Sort parameter for the list. It takes a column to order by and a direction (asc | desc).',
    isArray: true,
  })
  @Transform((params) => params.value.split(',').map(String))
  @IsArray()
  @IsString({ each: true })
  @IsOptional()
  sort?: string[] = ['id', 'desc'];

  get orderBy(): string {
    return this.sort[0];
  }

  get direction(): Direction {
    return this.sort[1] === 'desc' ? Direction.DESC : Direction.ASC;
  }
}

export class PageableMetaDTO {
  @ApiProperty()
  readonly page: number;

  @ApiProperty()
  readonly limit: number;

  @ApiProperty()
  readonly totalCount: number;

  @ApiProperty()
  readonly pageCount: number;

  @ApiProperty()
  readonly hasPreviousPage: boolean;

  @ApiProperty()
  readonly hasNextPage: boolean;

  constructor(params: PageQueryParamDTO, totalCount: number) {
    this.page = params.page;
    this.limit = params.limit;
    this.totalCount = totalCount;
    this.pageCount = Math.ceil(this.totalCount / this.limit);
    this.hasPreviousPage = this.page > 1;
    this.hasNextPage = this.page < this.pageCount;
  }
}

export class PageableDTO<T> {
  @IsArray()
  @ApiProperty({ isArray: true })
  readonly data: T[];

  @ApiProperty({ type: () => PageableMetaDTO })
  readonly meta: PageableMetaDTO;

  constructor(data: T[], meta: PageableMetaDTO) {
    this.data = data;
    this.meta = meta;
  }
}
