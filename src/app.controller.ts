import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags()
@Controller({
  version: '1',
})
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiOperation({ summary: 'Ping the backend to check it is running.' })
  @ApiOkResponse({
    description: 'Backend is running well.',
    type: String,
  })
  @Get('ping')
  pingPong(): string {
    return this.appService.pingPong();
  }
}
