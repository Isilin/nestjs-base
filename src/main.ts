import {
  BadRequestException,
  ValidationError,
  ValidationPipe,
  VersioningType,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder } from '@nestjs/swagger';
import { SwaggerModule } from '@nestjs/swagger/dist';
import * as compression from 'compression';
import * as fs from 'fs';
import * as path from 'path';
import helmet from 'helmet';
import { AppModule } from './app.module';
import LoggerService from './logging/service/logger.service';
import { EmptyResultInterceptor } from './_shared/interceptor/empty-result.interceptor';
import { NotFoundInterceptor } from './_shared/interceptor/not-found.interceptor';
import { HttpExceptionFilter } from './_shared/filter/http-exception.filter';

declare const module: any;

async function bootstrap() {
  // we need to create a dummy app to access the ConfigService that parse the .env to initialize
  // the https config of the real app.
  // TODO : find a better way of initializing the https config from ConfigService.
  const dummy = await NestFactory.create(AppModule);
  dummy.enableShutdownHooks();
  const configService = dummy.get(ConfigService);
  const sslEnabled = configService.get('sslEnabled');
  let httpsOptions = null;
  if (sslEnabled) {
    httpsOptions = {
      key: fs.readFileSync(path.join(__dirname, configService.get('sslKey'))),
      cert: fs.readFileSync(path.join(__dirname, configService.get('sslCert'))),
    };
  }

  const app = await NestFactory.create(AppModule, {
    cors: true,
    logger: ['error', 'warn', 'log', 'verbose', 'debug'],
    bufferLogs: true,
    httpsOptions: httpsOptions,
  });

  const rootPrefixe = configService.get('rootPrefixe');
  app.use(compression());
  app.use(helmet());
  app.setGlobalPrefix(rootPrefixe);
  app.enableVersioning({ type: VersioningType.URI });
  app.useLogger(app.get(LoggerService));

  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true, // to use dto as query params.
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        return new BadRequestException(validationErrors);
      },
    }),
  );
  app.useGlobalInterceptors(new EmptyResultInterceptor());
  app.useGlobalInterceptors(new NotFoundInterceptor());

  const config = new DocumentBuilder()
    .setTitle('NestJS Base')
    .setDescription(
      'A base NestJS project, with usual features to develop a strong API.',
    )
    .setVersion('0.1.0')
    .addTag('')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup(rootPrefixe, app, document, {
    swaggerOptions: {
      defaultModelsExpandDepth: -1,
      displayOperationId: true,
      docExpansion: 'none',
      filter: true,
    },
  });

  const port = configService.get('port');

  await app.listen(port);
  const loggerService = app.get(LoggerService);
  loggerService.log(
    `Application is running on: ${await app.getUrl()}.`,
    'Bootstrap',
  );

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
