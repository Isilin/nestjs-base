import {
  ConsoleLogger,
  ConsoleLoggerOptions,
  Injectable,
} from '@nestjs/common';

@Injectable()
export class LoggerService extends ConsoleLogger {
  constructor(context: string, options: ConsoleLoggerOptions) {
    super(context, {
      ...options,
      logLevels: ['error', 'warn', 'log', 'verbose', 'debug'],
    });
  }

  log(message: string, context?: string) {
    super.log.apply(this, [message, context]);
  }

  error(message: string, stack?: string, context?: string) {
    super.error.apply(this, [message, stack, context]);
  }

  warn(message: string, context?: string) {
    super.warn.apply(this, [message, context]);
  }

  debug(message: string, context?: string) {
    super.debug.apply(this, [message, context]);
  }

  verbose(message: string, context?: string) {
    super.verbose.apply(this, [message, context]);
  }
}

export default LoggerService;
