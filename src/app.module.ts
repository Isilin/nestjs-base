import { MiddlewareConsumer, ValidationPipe } from '@nestjs/common';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LoggerModule } from './logging/logger.module';
import LoggerMiddleware from './logging/middleware/logger.middleware';
import { configuration } from '../config/configuration';
import { validationSchema } from '../config/validation';
import { APP_FILTER, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { EmptyResultInterceptor } from './_shared/interceptor/empty-result.interceptor';
import { NotFoundInterceptor } from './_shared/interceptor/not-found.interceptor';
import { HttpExceptionFilter } from './_shared/filter/http-exception.filter';

@Module({
  imports: [
    LoggerModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `${process.cwd()}/config/env/.${process.env.NODE_ENV}.env`,
      load: [configuration],
      validationSchema,
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: EmptyResultInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: NotFoundInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
